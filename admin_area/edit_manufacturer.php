<?php
if(isset($_GET['edit_manufacturer'])){
    $edit_id=$_GET['edit_manufacturer'];
    $get_data="select * from `manufacturers` where id_manufacturer=$edit_id";
    $result=mysqli_query($con, $get_data);
    $row=mysqli_fetch_assoc($result);
    $name_manufacturer=$row['name_manufacturer'];
    $brief_information=$row['brief_information'];
}
?>

<div class="container mt-5">
    <h1 class="text-center">Редактировать сведения о производителе</h1>
    <form action="" method="post" enctype="multipart/form-data">
        <!-- Название -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="name_manufacturer" class="form-label">Производитель</label>
            <input type="text" name="name_manufacturer" id="name_manufacturer" class="form-control" value="<?php echo $name_manufacturer?>" required="required">
        </div>
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="brief_information" class="form-label">Краткая информация</label>
            <input type="text" name="brief_information" id="brief_information" class="form-control" value="<?php echo $brief_information?>" required="required">
        </div>
        <div class="w-50 m-auto">
            <input type="submit" name="edit_manufacturer" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Изменить">
        </div>
    </form>
</div>
<?php
if(isset($_POST['edit_manufacturer'])){
    $name_manufacturer=$_POST['name_manufacturer'];
    $brief_information=$_POST['brief_information'];
    if($name_manufacturer=='' or $brief_information==''){
        echo "<script>alert('Заполните все поля')</script>";
        exit();
    }
    else{
        $update_manufacturer="update `manufacturers` set name_manufacturer='$name_manufacturer', brief_information='$brief_information' where id_manufacturer=$edit_id";
        $result_update=mysqli_query($con,$update_manufacturer);
        if($result_update){
            echo "<script>alert('Информация о производителе успешно изменена')</script>";
            echo "<script>window.open('./admin_panel.php', '_self')</script>";
        }
    }
}
?>