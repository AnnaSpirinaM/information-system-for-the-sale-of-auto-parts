<?php
session_start();
if (!isset($_SESSION['email_admin']))
{
    echo "<script>window.open('./admin_login.php', '_self')</script>";
}
else
{
    echo "<script>window.open('./admin_panel.php', '_self')</script>";   
}
?>