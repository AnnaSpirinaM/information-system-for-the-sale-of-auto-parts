<?php
if(isset($_GET['edit_goods'])){
    $edit_id=$_GET['edit_goods'];
    $get_data="select * from `goods` where id_item=$edit_id";
    $result=mysqli_query($con, $get_data);
    $row=mysqli_fetch_assoc($result);
    $name_item=$row['name_item'];
    $characteristic=$row['characteristic'];
    $id_category=$row['id_category'];
    $id_manufacturer=$row['id_manufacturer'];
    $price=$row['price'];
    $image_now=$row['image'];
    $quantity=$row['quantity'];

    $select_category="select * from `categories` where id_category=$id_category";
    $result_category=mysqli_query($con, $select_category);
    $row_category=mysqli_fetch_assoc($result_category);
    $name_category=$row_category['name_category'];

    $select_manufacurer="select * from `manufacturers` where id_manufacturer=$id_manufacturer";
    $result_manuf=mysqli_query($con, $select_manufacurer);
    $row_manuf=mysqli_fetch_assoc($result_manuf);
    $name_manufacturer=$row_manuf['name_manufacturer'];
}
?>

<div class="container mt-5">
    <h1 class="text-center">Редактировать товар</h1>
    <form action="" method="post" enctype="multipart/form-data">
        <!-- Название -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="name_item" class="form-label">Название товара</label>
            <input type="text" name="name_item" id="name_item" class="form-control" value="<?php echo $name_item?>" required="required">
        </div>
        <!-- Описание -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="characteristic" class="form-label">Описание товара</label>
            <input type="text" name="characteristic" id="characteristic" class="form-control" value="<?php echo $characteristic?>" required="required">
        </div>
        <!-- Категория -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="name_category" class="form-label">Категория товара</label>
            <select name="name_category" class="form-select">
                <option value="<?php echo $id_category?>"><?php echo $name_category?></option>
                <?php
                $select_query="select * from `categories` order by name_category asc";
                $result_query=mysqli_query($con, $select_query);
                while($row=mysqli_fetch_assoc($result_query))
                {
                    $name_category=$row['name_category'];
                    $id_category=$row['id_category'];
                    echo "<option value='$id_category'>$name_category</option>";
                }
                ?>
            </select>
        </div>
        <!-- Производитель -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="name_manufacturer" class="form-label">Производитель</label>
            <select name="name_manufacturer" class="form-select">
                <option value="<?php echo $id_manufacturer?>"><?php echo $name_manufacturer?></option>
                <?php
                $select_query="select * from `manufacturers` order by name_manufacturer asc";
                $result_query=mysqli_query($con, $select_query);
                while($row=mysqli_fetch_assoc($result_query))
                {
                    $name_manufacturer=$row['name_manufacturer'];
                    $id_manufacturer=$row['id_manufacturer'];
                    echo "<option value='$id_manufacturer'>$name_manufacturer</option>";
                }
                ?>
            </select>
        </div>
        <!-- Изображение товара -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="image" class="form-label">Изображение товара</label>
            <div class="d-flex">
                <input type="file" name="image" id="image" class="form-control w-90 m-auto" accept=".png, .jpg, .jpeg">
                <img src="./goods_images/<?php echo $image_now?>" alt="" class="goods_images">
            </div>
        </div>
        <!-- Цена товара -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="price" class="form-label">Стоимость товара</label>
            <input type="number" min="1" max="20000" name="price" id="price" class="form-control" value="<?php echo $price?>" required="required">
        </div>
        <!-- Количество товара -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="quantity" class="form-label">Количество</label>
            <input type="number" min="0" max="100" name="quantity" id="quantity" class="form-control" value="<?php echo $quantity?>" required="required">
        </div>
        <div class="w-50 m-auto">
            <input type="submit" name="edit_item" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Изменить">
        </div>
    </form>
</div>
<?php
include('../includes/connect.php');
if(isset($_POST['edit_item'])){
    $name_item=$_POST['name_item'];
    $characteristic=$_POST['characteristic'];
    $name_category=$_POST['name_category'];
    $name_manufacturer=$_POST['name_manufacturer'];
    $price=$_POST['price'];
    $quantity=$_POST['quantity'];

    //доступ к изображению
    $image=$_FILES['image']['name'];
    //временное имя доступа к изображению
    $temp_image=$_FILES['image']['tmp_name'];
    if ($image=='')
    {
        $update_goods="update `goods` set id_category='$name_category', id_manufacturer='$name_manufacturer', name_item='$name_item', characteristic='$characteristic', price=$price, quantity=$quantity where id_item=$edit_id";
        $result_update=mysqli_query($con,$update_goods);
        if($result_update){
            echo "<script>alert('Информация о товаре успешно изменена')</script>";
            echo "<script>window.open('./admin_panel.php?view_goods', '_self')</script>";
        }
    }
    else
    {
        move_uploaded_file($temp_image, "./goods_images/$image");
        $update_goods="update `goods` set id_category='$name_category', id_manufacturer='$name_manufacturer', name_item='$name_item', characteristic='$characteristic', price=$price, quantity=$quantity, image='$image' where id_item=$edit_id";
        $result_update=mysqli_query($con,$update_goods);
        if($result_update){
            echo "<script>alert('Информация о товаре успешно изменена')</script>";
            echo "<script>window.open('./admin_panel.php?view_goods', '_self')</script>";
        }
    }
}
?>