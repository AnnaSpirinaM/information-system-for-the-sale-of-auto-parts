<h1 class="text-center">Категории</h1>
<table class="table table-bordered mt-3">
<thead class="text-color white" style="background-color: #4682B4; color: white">
    <tr>
        <th>№</th>
        <th>Название категории</th>
        <th>Редактировать</th>
    </tr>
    <tbody>
        <tr class="text-center">
            <?php
            $get_categories="select * from `categories` order by `name_category` asc";
            $rezult=mysqli_query($con, $get_categories);
            $count=0;
            while($row=mysqli_fetch_assoc($rezult))
            {
                $count++;
                $id_category=$row['id_category'];
                $name_category=$row['name_category'];
                echo "<tr><th>$count</th>
                <th>$name_category</th>
                <th><a href='admin_panel.php?edit_category=$id_category'><i class='fa-solid fa-pen-to-square'></i></a></th></tr>";
            }
            ?>
    </tbody>
</table>