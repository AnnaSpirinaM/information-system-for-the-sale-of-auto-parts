<h1 class="text-center">Все товары</h1>
<table class="table table-bordered mt-5">
    <thead style="background-color: #4682B4; color: white">
        <tr>
            <th>№</th>
            <th>Название товара</th>
            <th>Фото товара</th>
            <th>Стоимость</th>
            <th>Количество</th>
            <th>Редактировать</th>
        </tr>
    </thead>
    <tbody>
        <tr class="text-center">
            <?php
            $get_goods="select * from `goods`";
            $rezult=mysqli_query($con, $get_goods);
            $count=0;
            while($row=mysqli_fetch_assoc($rezult))
            {
                $count++;
                $id_item=$row['id_item'];
                $name_item=$row['name_item'];
                $image=$row['image'];
                $price=$row['price'];
                $quantity=$row['quantity'];
                echo "
                <tr class='text-center'>
                    <th>$count</th>
                    <th>$name_item</th>
                    <th><img src='./goods_images/$image' class='goods_images'/></th>
                    <th>$price</th>
                    <th>$quantity</th>
                    <th><a href='admin_panel.php?edit_goods=$id_item'><i class='fa-solid fa-pen-to-square'></i></a></th>
                </tr>";
            }
            ?>
        </tr>
    </tbody>
</table>