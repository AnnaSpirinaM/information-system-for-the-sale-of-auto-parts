<h1 class="text-center">Производители</h1>
<table class="table table-bordered mt-3">
<thead style="background-color: #4682B4; color: white">
    <tr>
        <th>№</th>
        <th>Производитель</th>
        <th>Краткая информация</th>
        <th>Редактировать</th>
    </tr>
    <tbody>
        <tr class="text-center">
            <?php
            $get_manufacturers="select * from `manufacturers` order by `name_manufacturer` asc";
            $rezult=mysqli_query($con, $get_manufacturers);
            $count=0;
            while($row=mysqli_fetch_assoc($rezult))
            {
                $count++;
                $id_manufacturer=$row['id_manufacturer'];
                $name_manufacturer=$row['name_manufacturer'];
                $brief_information=$row['brief_information'];
                echo "<tr><th>$count</th>
                <th>$name_manufacturer</th>
                <th>$brief_information</th>
                <th><a href='admin_panel.php?edit_manufacturer=$id_manufacturer'><i class='fa-solid fa-pen-to-square'></i></a></th></tr>";
            }
            ?>
    </tbody>
</table>