<?php
include('../includes/connect.php');
if(isset($_POST['insert_manu'])){
    $name_manufacturer=$_POST['name_manufacturer'];
    $brief_information=$_POST['brief_information'];
    
    $select_query="select * from `manufacturers` where name_manufacturer='$name_manufacturer'";
    $result_select=mysqli_query($con, $select_query);
    $num=mysqli_num_rows($result_select);
    if($num>0)
    {
        echo "<script>alert('Данный производитель уже присутствует в базе данных')</script>";
    }
    else{
        $insert_query="insert into `manufacturers` (name_manufacturer, brief_information) values ('$name_manufacturer', '$brief_information')";
        $result=mysqli_query($con, $insert_query);
        if($result){
            echo "<script>alert('Производитель успешно добавлен')</script>";
        }
    }
}
?>

<h2 class="text-center">Добавить производителя</h2>
<form action="" method="post" class="mb-2">
    <div class="input-group w-90 mb-2">
        <span class="input-group-text" style="background-color: #4682B4" id="basic-addon1"></span>
        <input type="text" class="form-control" name="name_manufacturer" placeholder="Введите название компании-производителя" aria-describedby="basic-addon1" required="required">
    </div>
    <div class="input-group mb-2">
        <span class="input-group-text">Краткая информация</span>
        <textarea class="form-control" name="brief_information" required="required" aria-label="With textarea"></textarea>
    </div>
    <div class="input-group w-10 mb-2 m-auto">
        <input type="submit" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Добавить" name="insert_manu">
    </div>
</form>