<?php
if(isset($_GET['edit_category'])){
    $edit_id=$_GET['edit_category'];
    $get_data="select * from `categories` where id_category=$edit_id";
    $result=mysqli_query($con, $get_data);
    $row=mysqli_fetch_assoc($result);
    $name_category=$row['name_category'];
}
?>

<div class="container mt-5">
    <h1 class="text-center">Редактировать название категории</h1>
    <form action="" method="post" enctype="multipart/form-data">
        <!-- Название -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="name_category" class="form-label">Название категории</label>
            <input type="text" name="name_category" id="name_category" class="form-control" value="<?php echo $name_category?>" required="required">
        </div>
        <div class="w-50 m-auto">
            <input type="submit" name="edit_category" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Изменить">
        </div>
    </form>
</div>
<?php
if(isset($_POST['edit_category'])){
    $name_category=$_POST['name_category'];
    if($name_category==''){
        echo "<script>alert('Заполните все поля')</script>";
        exit();
    }
    else{
        $update_category="update `categories` set name_category='$name_category' where id_category=$edit_id";
        $result_update=mysqli_query($con,$update_category);
        if($result_update){
            echo "<script>alert('Информация о категории успешно изменена')</script>";
            echo "<script>window.open('./admin_panel.php', '_self')</script>";
        }
    }
}
?>