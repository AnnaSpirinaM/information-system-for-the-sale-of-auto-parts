<?php
include('../includes/connect.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Вход администратора</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- font awesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- css file -->
    <link rel="stylesheet" href="style.css">
</head>
<body style="font-family: Arial; overflow-x: hidden">
    <div class="container-fluid m-3">
        <h2 class="text-center mb-5">Вход</h2>
        <form action="" method="post">
            <div class="form-outline mb-4 w-50 m-auto">
                <label for="email_admin" class="form-label">E-mail</label>
                <input type="email" id="email_admin" class="form-control" placeholder="Введите адрес электронной почты" autocomplete="off" required="required" name="email_admin"/>
            </div>
            <div class="form-outline mb-4 w-50 m-auto">
                <label for="password" class="form-label">Пароль</label>
                <input type="password" id="password" name="password" placeholder="Введите пароль" required="required" class="form-control">
            </div>
            <div class="w-50 m-auto">
                <input type="submit" name="admin_login" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Войти">
            </div>
        </form>
    </div>
<!-- bootstrap js link -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

<?php
if(isset($_POST['admin_login']))
{
    $email_admin = $_POST['email_admin'];
    $password = $_POST['password'];
    $select_query="select * from `administration` where email_admin='$email_admin'";
    $result = mysqli_query($con, $select_query);
    $row_count = mysqli_num_rows($result);
    $row_data = mysqli_fetch_assoc($result);
    if ($row_count >0)
    {
        if ($password == $row_data['password'])
        {
            $_SESSION['email_admin'] = $email_admin;
            echo "<script>alert('Успешный вход в систему')</script>";
            echo "<script>window.open('./admin_panel.php', '_self')</script>";
        }
        else
        {
            echo "<script>alert('Неверно введён пароль')</script>";
        }
    }
    else
    {
        echo "<script>alert('Администратор с введённой электронной почтой не найден')</script>";
    }
}
?>