<?php
include('../includes/connect.php');
if(isset($_POST['insert_item'])){
    $name_item=$_POST['name_item'];
    $characteristic=$_POST['characteristic'];
    $name_category=$_POST['name_category'];
    $name_manufacturer=$_POST['name_manufacturer'];
    $price=$_POST['price'];
    $quantity=$_POST['quantity'];

    //доступ к изображению
    $image=$_FILES['image']['name'];
    //временное имя доступа к изображению
    $temp_image=$_FILES['image']['tmp_name'];

    if($name_category=='' or $name_manufacturer==''){
        echo "<script>alert('Заполните все поля')</script>";
    }
    else{
        move_uploaded_file($temp_image, "./goods_images/$image");
        $insert_goods="insert `goods` (id_category, id_manufacturer, name_item, characteristic, price, image, quantity) values ('$name_category', '$name_manufacturer', '$name_item', '$characteristic', $price, '$image', $quantity)";
        $result_query=mysqli_query($con,$insert_goods);
        if($result_query){
            echo "<script>alert('Товар успешно добавлен')</script>";
        }
    }
}
?>
<div class="container mt-3">
    <h1 class="text-center">Добавить товар</h1>
    <!-- form -->
    <form action="" method="post" enctype="multipart/form-data">
        <!-- Название -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="" class="form-label">Название товара</label>
            <input type="text" name="name_item" id="name_item" class="form-control" placeholder="Введите название товара" required="required">
        </div>
        <!-- Описание -->
        <div class="input-group mb-4 w-50 m-auto">
            <span class="input-group-text">Описание товара</span>
            <textarea class="form-control" name="characteristic" id="characteristic" placeholder="Введите описание товара" required="required"></textarea>
        </div>
        <!-- Категория -->
        <div class="form-outline mb-4 w-50 m-auto">
            <select name="name_category" id="" class="form-select">
                <option value="">Выберите категорию</option>
                <?php
                $select_query="select * from `categories` order by name_category asc";
                $result_query=mysqli_query($con, $select_query);
                while($row=mysqli_fetch_assoc($result_query))
                {
                    $name_category=$row['name_category'];
                    $id_category=$row['id_category'];
                    echo "<option value='$id_category'>$name_category</option>";
                }
                ?>
            </select>
        </div>
        <!-- Производитель -->
        <div class="form-outline mb-4 w-50 m-auto">
            <select name="name_manufacturer" id="" class="form-select">
                <option value="">Выберите производителя</option>
                <?php
                $select_query="select * from `manufacturers` order by name_manufacturer asc";
                $result_query=mysqli_query($con, $select_query);
                while($row=mysqli_fetch_assoc($result_query))
                {
                    $name_manufacturer=$row['name_manufacturer'];
                    $id_manufacturer=$row['id_manufacturer'];
                    echo "<option value='$id_manufacturer'>$name_manufacturer</option>";
                }
                ?>
            </select>
        </div>
        <!-- Изображение товара -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="image" class="form-label">Изображение товара</label>
            <input type="file" name="image" accept=".png, .jpg, .jpeg" id="image" class="form-control" required="required">
        </div>
        <!-- Цена товара -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="price" class="form-label">Стоимость товара</label>
            <input type="number" min="1" max="20000" name="price" id="price" class="form-control" placeholder="Введите стоимость товара" required="required">
        </div>
        <!-- Количество товара -->
        <div class="form-outline mb-4 w-50 m-auto">
            <label for="quantity" class="form-label">Количество</label>
            <input type="number" min="0" max="100" name="quantity" id="quantity" class="form-control" placeholder="Введите количество товара" required="required">
        </div>
        <div class="form-outline mb-4 w-50 m-auto">
            <input type="submit" name="insert_item" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Добавить">
        </div>
    </form>
</div>