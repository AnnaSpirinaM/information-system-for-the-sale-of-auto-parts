<?php
include('../includes/connect.php');
session_start();
if (!isset($_SESSION['email_admin']))
{
    echo "<h1 class='text-center'>Доступа к странице нет</h1>";
}
else
{
    echo
    "
    <!DOCTYPE html>
    <html lang='en'>
    <head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE-edge'>
    <meta name='viewport' content='width-device-width, initial-scale=1.0'>
    <!-- bootstrap CSS link -->
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
    <!-- font awesome link -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css' integrity='sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==' crossorigin='anonymous' referrerpolicy='no-referrer' />
    <!-- css file -->
    <link rel='stylesheet' href='../style.css'>
    <style>
    .goods_images{
        width: 100px;
        object-fit:contain
    }
    </style>
</head>
<body style='font-family: Arial; overflow-x: hidden'>
    <!-- navbar -->
    <div class='container-fluid p-0'>
        <!-- first part -->
        <nav class='navbar navbar-expand-lg navbar-light' style='background-color: #4682B4'>
            <div class='container-fluid'>
                <a class='nav-link' href='./admin_logout.php'><img src='../images/выход.png' width='50' height='50'></a>'
            </div>
        </nav>

        <!-- second part -->
        <div class='bg-light'>
            <h3 class='text-center p-2'>Панель управления</h3>
        </div>

        <!-- third part -->
        <div class='row'>
            <div class='col-md-12 bg-light p-1 align-items-center'>
                <div class='button text-center'>
                    <div class='dropdown'>
                        <button class='btn dropdown-toggle text-white' style='background-color: #4682B4' type='button' id='dropdownMenuButton1' data-bs-toggle='dropdown' aria-expanded='false'>
                        Товары
                        </button>
                        <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton1'>
                            <li><a class='dropdown-item' href='admin_panel.php?insert_goods'>Добавить</a></li>
                            <li><a class='dropdown-item' href='admin_panel.php?view_goods'>Показать</a></li>
                        </ul>
                        <button class='btn dropdown-toggle text-white' style='background-color: #4682B4' type='button' id='dropdownMenuButton1' data-bs-toggle='dropdown' aria-expanded='false'>
                        Категории
                        </button>
                        <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton1'>
                            <li><a class='dropdown-item' href='admin_panel.php?insert_category'>Добавить</a></li>
                            <li><a class='dropdown-item' href='admin_panel.php?view_categories'>Показать</a></li>
                        </ul>
                        <button class='btn dropdown-toggle text-white' style='background-color: #4682B4' type='button' id='dropdownMenuButton1' data-bs-toggle='dropdown' aria-expanded='false'>
                        Производители
                        </button>
                        <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton1'>
                            <li><a class='dropdown-item' href='admin_panel.php?insert_manufacturer'>Добавить</a></li>
                            <li><a class='dropdown-item' href='admin_panel.php?view_manufacturers'>Показать</a></li>
                        </ul>
                        <button class='btn dropdown-toggle text-white' style='background-color: #4682B4' type='button' id='dropdownMenuButton1' data-bs-toggle='dropdown' aria-expanded='false'>
                        Заказы
                        </button>
                        <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton1'>
                            <li><a class='dropdown-item' href='admin_panel.php?assembly'>На сборке</a></li>
                            <li><a class='dropdown-item' href='admin_panel.php?received'>Полученные</a></li>
                            <li><a class='dropdown-item' href='admin_panel.php?cancelled'>Отменённые</a></li>
                            <li><a class='dropdown-item' href='admin_panel.php?ready'>Готовы к выдаче</a></li>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- fourth part -->
        <div class='container my-3'>";
            if(isset($_GET['insert_goods'])){
                include('insert_goods.php');
            }
            if(isset($_GET['view_goods'])){
                include('view_goods.php');
            }
            if(isset($_GET['insert_category'])){
                include('insert_category.php');
            }
            if(isset($_GET['insert_manufacturer'])){
                include('insert_manufacturer.php');
            }
            if(isset($_GET['edit_goods'])){
                include('edit_goods.php');
            }
            if(isset($_GET['view_categories'])){
                include('view_categories.php');
            }
            if(isset($_GET['view_manufacturers'])){
                include('view_manufacturers.php');
            }
            if(isset($_GET['edit_category'])){
                include('edit_category.php');
            }
            if(isset($_GET['edit_manufacturer'])){
                include('edit_manufacturer.php');
            }
            if(isset($_GET['assembly'])){
                include('assembly.php');
            }
            if(isset($_GET['received'])){
                include('received.php');
            }
            if(isset($_GET['cancelled'])){
                include('cancelled.php');
            }
            if(isset($_GET['ready'])){
                include('ready.php');
            }
            if(isset($_GET['viewing_assembly']))
            {
                include('viewing_assembly.php');
            }
            if(isset($_GET['viewing_ready']))
            {
                include('viewing_ready.php');
            }
            if(isset($_GET['viewing_received']))
            {
                include('viewing_received.php');
            }
            if(isset($_GET['ready_status']))
            {
                $id_order=$_GET['ready_status'];
                $update_status="update `orders` SET status=4 where id_order=$id_order";
                $result_update=mysqli_query($con, $update_status);
                $select_query = "select date, email_user from `orders` where id_order=$id_order";
                $result_query=mysqli_query($con, $select_query);
                $run_query = mysqli_fetch_array($result_query);
                if($result_update){
                    $email_user = $run_query['email_user'];
                    $date = $run_query['date'];
                    $select1 = "select * from `orders` where email_user='$email_user' and status=1 and date='$date'";
                    $result=mysqli_query($con, $select1);
                    $run = mysqli_fetch_array($result);
                    if ($run != null)
                    {
                        echo "<script>window.open('./admin_panel.php?viewing_assembly=$date $email_user', '_self')</script>";
                    }
                    else
                    {
                        echo "<script>window.open('./admin_panel.php?assembly', '_self')</script>";
                    }
                }
            }
            if(isset($_GET['cancel_status']))
            {
                $id_order=$_GET['cancel_status'];
                $update_status="update `orders` SET status=3 where id_order=$id_order";
                $result_update=mysqli_query($con, $update_status);
                $select_goods="select * from `orders` where id_order=$id_order";
                $result_query=mysqli_query($con, $select_goods);
                $run_query = mysqli_fetch_array($result_query);
                $id_item=$run_query['id_item'];
                $quantity=$run_query['quantity'];
                $update_goods="update `goods` set quantity=quantity+$quantity where id_item=$id_item";
                $result=mysqli_query($con, $update_goods);
                $select_query = "select date, email_user from `orders` where id_order=$id_order";
                $result_query=mysqli_query($con, $select_query);
                $run_query = mysqli_fetch_array($result_query);
                if($result_update){
                    $email_user = $run_query['email_user'];
                    $date = $run_query['date'];
                    $select1 = "select * from `orders` where email_user='$email_user' and status=1 and date='$date'";
                    $result=mysqli_query($con, $select1);
                    $run = mysqli_fetch_array($result);
                    if ($run != null)
                    {
                        echo "<script>window.open('./admin_panel.php?viewing_assembly=$date $email_user', '_self')</script>";
                    }
                    else
                    {
                        echo "<script>window.open('./admin_panel.php?assembly', '_self')</script>";
                    }
                }
            }
            if(isset($_GET['receive_status']))
            {
                $id_order=$_GET['receive_status'];
                $update_status="update `orders` SET status=2 where id_order=$id_order";
                $result_update=mysqli_query($con, $update_status);
                $select_query = "select date, email_user from `orders` where id_order=$id_order";
                $result_query=mysqli_query($con, $select_query);
                $run_query = mysqli_fetch_array($result_query);
                if($result_update){
                    $email_user = $run_query['email_user'];
                    $date = $run_query['date'];
                    $select1 = "select * from `orders` where email_user='$email_user' and status=4 and date='$date'";
                    $result=mysqli_query($con, $select1);
                    $run = mysqli_fetch_array($result);
                    if ($run != null)
                    {
                        echo "<script>window.open('./admin_panel.php?viewing_ready=$date $email_user', '_self')</script>";
                    }
                    else
                    {
                        echo "<script>window.open('./admin_panel.php?ready', '_self')</script>";
                    }
                }
            }
        echo "</div>
    <!-- bootstrap js link -->
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
</body>
</html>";
}
?>