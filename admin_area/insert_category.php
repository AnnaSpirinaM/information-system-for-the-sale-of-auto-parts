<?php
include('../includes/connect.php');
if(isset($_POST['insert_cat'])){
    $name_category=$_POST['name_category'];
    
    $select_query="select * from `categories` where name_category='$name_category'";
    $result_select=mysqli_query($con, $select_query);
    $num=mysqli_num_rows($result_select);
    if($num>0)
    {
        echo "<script>alert('Данная категория уже присутствует в базе данных')</script>";
    }
    else{
        $insert_query="insert into `categories` (name_category) values ('$name_category')";
        $result=mysqli_query($con, $insert_query);
        if($result){
            echo "<script>alert('Категория добавлена успешно')</script>";
        }
    }
}
?>

<h2 class="text-center">Добавить категорию</h2>
<form action="" method="post" class="mb-2">
    <div class="input-group w-90 mb-2">
        <span class="input-group-text" style="background-color: #4682B4" id="basic-addon1"></span>
        <input type="text" class="form-control" name="name_category" placeholder="Введите название категории" required="required" aria-describedby="basic-addon1">
    </div>
    <div class="input-group w-10 mb-2 m-auto">
        <input type="submit" name="insert_cat" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Добавить">
    </div>
</form>