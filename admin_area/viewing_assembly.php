<?php
if(isset($_GET['viewing_assembly'])){
    $info=$_GET['viewing_assembly'];
    $pieces = explode(" ", $info);
    $date=$pieces[0] . " ". $pieces[1];
    $email_user=$pieces[2];
}
?>

<h1 class="text-center">Подробная информация о заказе</h1>
<h3><strong>О клиенте:</strong></h3>
<?php
$select_query="select * from `users` where email_user='$email_user'";
$result_query=mysqli_query($con, $select_query);
$run_query = mysqli_fetch_array($result_query);
$name_user=$run_query['name_user'];
$surname_user=$run_query['surname_user'];
$mobile_user=$run_query['mobile_user'];
echo "<div class='container-fluid'>
<p>$name_user $surname_user</p>
<p>Электронная почта: $email_user</p>
<p>Номер телефона: $mobile_user</p>
<h3><strong>Дата заказа: $date</strong></h3>
</div>"
?>
<table class="table table-bordered mt-3">
<thead class="text-color white" style="background-color: #4682B4; color: white">
    <tr>
        <th>№</th>
        <th>Товар</th>
        <th>Стоимость товара</th>
        <th>Количество</th>
        <th>Итог</th>
        <th>Статус</th>
    </tr>
    <tbody>
        <tr class="text-center">
            <?php
            $get_orders="select * from `orders` where status=1 and email_user='$email_user' and date='$date'";
            $rezult=mysqli_query($con, $get_orders);
            $count=0;
            while($row=mysqli_fetch_assoc($rezult))
            {
                $count++;
                $id_order=$row['id_order'];
                $id_item=$row['id_item'];

                $select_query="select * from `goods` where id_item='$id_item'";
                $result_query=mysqli_query($con, $select_query);
                $run_query = mysqli_fetch_array($result_query);
                $name_item=$run_query['name_item'];
                $price=$run_query['price'];

                $quantity=$row['quantity'];
                $total=$row['total'];
                $status=$row['status'];

                echo "<tr><th>$count</th>
                <th>$name_item</th>
                <th>$price</th>
                <th>$quantity</th>
                <th>$total</th>
                <th><button class='btn dropdown-toggle text-white' style='background-color: #4682B4' type='button' id='dropdownMenuButton1' data-bs-toggle='dropdown' aria-expanded='false'>
                Изменить
                </button>
                <ul class='dropdown-menu' aria-labelledby='dropdownMenuButton1'>
                <li><a class='dropdown-item' href='admin_panel.php?ready_status=$id_order'>Готов к выдаче</a></li>
                <li><a class='dropdown-item' href='admin_panel.php?cancel_status=$id_order'>Отменён</a></li>
                </button></th>
                </tr>";
            }
            ?>
    </tbody>
</table>