<?php
include('includes/connect.php');
include('functions/common_function.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Контактная информация | АВТОМИР</title>
    <!-- bootstrap CSS link -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- font awesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- css file -->
    <link rel="stylesheet" href="style.css">
</head>
<body style="font-family: Arial; overflow-x: hidden">
    <!-- navbar -->
    <div class="container-fluid p-0">
        <!-- first part -->
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #4682B4">
            <div class="container-fluid">
                <a href="index.php"><img src="./images/логотип.png" alt="" width="50" height="50"></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px" href="index.php">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px" href="goods.php">Товары</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px; text-shadow: #FFFFFF 0 0 10px;" href="contacts.php">Контакты</a>
                        </li>
                    </ul>
                    <?php
                    if (!isset($_SESSION['email_user']))
                    {
                        echo "<a class='nav-link text-white' href='./user_area/user_login.php'><img src='./images/корзина.png' width='50' height='50'></a>
                        <a class='nav-link' href='./user_area/user_login.php'><img src='./images/личный кабинет.png' width='50' height='50'></a>";
                    }
                    else
                    {
                        echo "<a class='nav-link' href='profile.php'><img src='./images/профиль.png' width='50' height='50'></a>
                        <a class='nav-link text-white' href='cart.php'><img src='./images/корзина.png' width='50' height='50'>"; count_items(); echo "</a>
                        <a class='nav-link' href='./user_area/user_logout.php'><img src='./images/выход.png' width='50' height='50'></a>";
                    }
                    ?>
                    <form class="d-flex" action="search_item.php" method="get">
                        <input class="form-control me-2" type="search" placeholder="Поиск" aria-label="Search" name="search_data">
                        <input type="submit" value="Поиск" class="btn btn-outline-light" name="search_data_item">
                    </form>
                </div>
            </div>
        </nav>

        <!-- third part -->
        <div class="row px-3" style='margin-top:20px'>
            <div class="col-md-4">
                <h4 class='text-secondary'>Адрес</h4>
                <h5>г.Пермь, ул.Василия Васильева 17/1</h5>
            </div>
            <div class="col-md-2">
                <h4 class='text-secondary'>Режим работы</h4>
                <h5>пн-вс с 10.00 до 20.00</h5>
            </div>
            <div class="col-md-3">
                <h4 class='text-secondary'>Контакты</h4>
                <div>
                    <p><a href="tel:+79068777500"><img src="./images/телефон.png" alt="" width="50" height="50">+79068777500</a></p>
                    <p><a href="mailto:IPGalimovRH@gmail.com"><img src="./images/почта.png" alt="" width="50" height="50">IPGalimovRH@gmail.com</a></p>
                </div>
            </div>
            <div class="col-md-3">
                <h4 class='text-secondary'>Мы в социальных сетях</h4>
                <div><a href="https://vk.com/public218490604"><img src="./images/вк.png" alt="" width="50" height="50"></a></div>
            </div>
        </div>
        <div style='margin-top:20px'>
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A14a11bd47476fbccb6b4ff0568a973601b0fe66ba9232b071cb0ed6bf0091a12&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
        </div>
    <div class="container">
            <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
                <div class="col-md-4 d-flex align-items-center">
                <span class="text-muted">© 2023 Автомир</span>
                </div>
            </footer>
        </div>
<!-- bootstrap js link -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>