<?php

//все товары
function get_goods(){
    global $con;
    if(!isset($_GET['category'])){
        $select_query="select * from `goods`";
        $result_query=mysqli_query($con, $select_query);
        while($row=mysqli_fetch_assoc($result_query)){
            $id_item=$row['id_item'];
            $name_item=$row['name_item'];
            $id_manufacturer=$row['id_manufacturer'];
            $price=$row['price'];
            $image=$row['image'];
            $quantity=$row['quantity'];
            echo "
                <div class='col-md-3 mb-2' style='margin-top:20px'>
                    <div class='card' style='height: 100%'>
                        <a href='item_details.php?id_item=$id_item'><img src='./admin_area/goods_images/$image' class='card-img-top' alt='$name_item'></a>
                        <div class='card-body'>
                            <h5 class='card-title'>$name_item</h5>
                            <p class='card-text'>$price ₽</p>";
                            if(!isset($_SESSION['email_user']))
                            {
                                echo "<a href='./user_area/user_login.php' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                            }
                            else
                            {
                                echo "<a href='goods.php?add_cart=$id_item' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                            }
                            echo "<a>  </a><a href='item_details.php?id_item=$id_item' class='btn' style='background-color: #808080; color: #FFFFFF'>Подробнее</a>
                        </div>
                    </div>
                </div>";
        }
    }
}

function get_categories(){
    global $con;
    $select_categories="select * from `categories` order by `name_category` asc";
    $result_categories=mysqli_query($con, $select_categories);
    while($row_data=mysqli_fetch_assoc($result_categories))
    {
        $name_category = $row_data['name_category'];
        $id_category = $row_data['id_category'];
        echo "<li class='nav-item' style='background-color: #4682B4; border: 2px solid #FFFFFF; border-radius: 15px; opacity: 0.8;'>
        <a href='goods.php?category=$id_category' class='nav-link text-white' style='margin-left:5px'>$name_category</a></li>";
    }
}

//все товары
function get_unique_category(){
    global $con;
    if(isset($_GET['category'])){
        $id_category=$_GET['category'];
        $select_query="select * from `goods` where id_category=$id_category";
        $result_query=mysqli_query($con, $select_query);
        $num=mysqli_num_rows($result_query);
        $select_category="select * from `categories` where id_category=$id_category";
        $result_category=mysqli_query($con, $select_category);
        while($row=mysqli_fetch_assoc($result_category))
        {
            $name_category=$row['name_category'];
            echo "<div style='margin-top:20px'>
                    <h3 class='text-center' style='color: #4682B4'>$name_category</h3>
                </div>";
        }
        if($num==0)
        {
            echo "<h2 class='text-center text-secondary'>Товаров в данной категории пока нет</h2>";
        }
        else
        {
            while($row=mysqli_fetch_assoc($result_query))
            {
                $id_item=$row['id_item'];
                $name_item=$row['name_item'];
                $id_manufacturer=$row['id_manufacturer'];
                $price=$row['price'];
                $image=$row['image'];
                $quantity=$row['quantity'];
                echo "
                        <div class='col-md-3 mb-2' style='margin-top:20px'>
                            <div class='card' style='height: 100%'>
                                <a href='item_details.php?id_item=$id_item'><img src='./admin_area/goods_images/$image' class='card-img-top' alt='$name_item'></a>
                                <div class='card-body'>
                                    <h5 class='card-title'>$name_item</h5>
                                    <p class='card-text'>$price ₽</p>";
                                    if(!isset($_SESSION['email_user']))
                                    {
                                        echo "<a href='./user_area/user_login.php' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                                    }
                                    else
                                    {
                                        echo "<a href='goods.php?add_cart=$id_item' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                                    }
                                    echo "<a>  </a><a href='item_details.php?id_item=$id_item' class='btn' style='background-color: #808080; color: #FFFFFF'>Подробнее</a>
                                </div>
                            </div>
                        </div>";
            }
        }
    }
}

//Поиск
function search_item(){
    global $con;
    if(isset($_GET['search_data_item']))
    {
        $search_data_value=$_GET['search_data'];
        $search_query="select * from `goods` where name_item like '%$search_data_value%'";
        $result_query=mysqli_query($con, $search_query);
        $num=mysqli_num_rows($result_query);
        if($num==0){
            echo "<div style='margin-top:20px'>
                    <h2 class='text-center text-secondary'>Товары не найдены</h2>
                </div>";
        }
        else{
            while($row=mysqli_fetch_assoc($result_query)){
                $id_item=$row['id_item'];
                $name_item=$row['name_item'];
                $characteristic=$row['characteristic'];
                $id_manufacturer=$row['id_manufacturer'];
                $price=$row['price'];
                $image=$row['image'];
                $quantity=$row['quantity'];
                echo "
                        <div class='col-md-3 mb-2' style='margin-top:20px'>
                            <div class='card' style='height: 100%'>
                                <a href='item_details.php?id_item=$id_item'><img src='./admin_area/goods_images/$image' class='card-img-top' alt='$name_item'></a>
                                <div class='card-body'>
                                    <h5 class='card-title'>$name_item</h5>
                                    <p class='card-text'>$price ₽</p>";
                                    if(!isset($_SESSION['email_user']))
                                    {
                                        echo "<a href='./user_area/user_login.php' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                                    }
                                    else
                                    {
                                        echo "<a href='goods.php?add_cart=$id_item' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                                    }
                                    echo "<a>  </a><a href='item_details.php?id_item=$id_item' class='btn' style='background-color: #808080; color: #FFFFFF'>Подробнее</a>
                                </div>
                            </div>
                        </div>";
                }
            }
        }
    }

function view_details()
{
    global $con;
    if(isset($_GET['id_item'])){
        if(!isset($_GET['category'])){
            $id_item=$_GET['id_item'];
            $select_query="select * from `goods` where id_item=$id_item";
            $result_query=mysqli_query($con, $select_query);
            while($row=mysqli_fetch_assoc($result_query)){
                $id_item=$row['id_item'];
                $name_item=$row['name_item'];
                $image=$row['image'];
                $price=$row['price'];
                $characteristic=$row['characteristic'];
                $id_manufacturer=$row['id_manufacturer'];
                $quantity=$row['quantity'];
                $select_manufacturer="select * from `manufacturers` where id_manufacturer=$id_manufacturer";
                $result_manufacturer=mysqli_query($con, $select_manufacturer);
                while($row=mysqli_fetch_assoc($result_manufacturer)){
                    $name_manufacturer=$row['name_manufacturer'];
                    $brief_information=$row['brief_information'];
                    echo "
                    <div>
                        <h1 class='text-center'>$name_item</h1>
                    </div>
                    <div class='row' style='margin-left:20px; margin-top:20px'>
                        <div class='col-md-4 mb-2'>
                            <div class='card'>
                                <img src='./admin_area/goods_images/$image' class='card-img-top' alt='$name_item'>
                                <div class='card-body'>
                                    <h5 class='card-title'>$name_item</h5>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-8 mb-2'>
                            <div class='row'>
                                <!-- Цена -->
                                <div class='col-md-3'>
                                    <h4 class='text-secondary'>Стоимость</h4>
                                    <h5>$price ₽</h5>";
                                    if(!isset($_SESSION['email_user']))
                                    {
                                        echo "<a href='./user_area/user_login.php' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                                    }
                                    else
                                    {
                                        echo "<a href='goods.php?add_cart=$id_item' class='btn' style='background-color: #4682B4; color: #FFFFFF'>В корзину</a>";
                                    }
                                echo "<p>В наличии: $quantity</p>
                                <a href='tel:+79068777500' class='btn' style='background-color: #4682B4; color: #FFFFFF'>Забронировать</a></div>
                                <!-- Характеристика -->
                                <div class='col-md-9'>
                                    <h4 class='text-secondary'>Информация о товаре</h4>
                                    <h5>$characteristic</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row' style='margin-top:50px'>
                        <div class='col-md-1 mb-2'>
                        </div>
                        <div class='col-md-10 mb-2'>
                            <h3>Производитель: $name_manufacturer</h3>
                            <p>$brief_information</p>
                        </div>
                        <div class='col-md-1 mb-2'>
                        </div>
                    </div>";
                }
            }
        }
    }
}

function get_promotions(){
    global $con;
    $select_query="select * from `promotions`";
    $result_query=mysqli_query($con, $select_query);
    return $result_query;
}

function make_slide_indicators(){
    global $con;
    $output = '';
    $count = 1;
    $result = get_promotions();
    while($row = mysqli_fetch_array($result))
    {
        if($count == 1)
        {
            $output .= '<button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="'.$count.'" class="active" aria-current="true"></button>';
        }
        else
        {
            $output .= '<button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="'.$count.'"></button>'; 
        }
        $count = $count + 1;
    }
    return $output;
}


function cart()
{
    if(isset($_GET['add_cart']))
    {
        if (isset($_SESSION['email_user']))
        {
            global $con;
            $email_user = $_SESSION['email_user'];
            $get_product_id = $_GET['add_cart'];
            $select_query = "select * from `cart` where email_user='$email_user' and id_item=$get_product_id";
            $result_query=mysqli_query($con, $select_query);
            $num=mysqli_num_rows($result_query);
            if($num>0){
                echo "<script>alert('Данный товар уже есть в корзине')</script>";
            }
            else
            {
                $select_goods = "select * from `goods` where id_item=$get_product_id";
                $result=mysqli_query($con, $select_goods);
                $run_query = mysqli_fetch_array($result);
                $quantity=$run_query['quantity'];
                if ($quantity>0)
                {
                    $insert_query = "insert into `cart` (email_user, id_item, quantity) values ('$email_user', $get_product_id, 1)";
                    $result_query=mysqli_query($con, $insert_query);
                    echo "<script>window.open('goods.php', '_self')</script>";
                }
                else
                {
                    echo "<script>alert('Товара нет в наличии')</script>";
                }
            }
        }
    }
}


function count_items()
{
    if (isset($_SESSION['email_user']))
    {
        if(isset($_GET['add_cart']))
        {
            global $con;
            $email_user = $_SESSION['email_user'];
            $select_query = "select * from cart where email_user='$email_user'";
            $result_query=mysqli_query($con, $select_query);
            $count=mysqli_num_rows($result_query);
        }
        else
        {
            global $con;
            $email_user = $_SESSION['email_user'];
            $select_query = "select * from cart where email_user='$email_user'";
            $result_query=mysqli_query($con, $select_query);
            $count=mysqli_num_rows($result_query);
        }
        echo $count;
    }
}
?>