<?php
include('includes/connect.php');
include('functions/common_function.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Товары | АВТОМИР</title>
    <!-- bootstrap CSS link -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- font awesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- css file -->
    <link rel="stylesheet" href="style.css">
</head>
<body style="font-family: Arial; overflow-x: hidden">
    <!-- navbar -->
    <div class="container-fluid p-0">
        <!-- first part -->
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #4682B4">
            <div class="container-fluid">
                <a href="index.php"><img src="./images/логотип.png" alt="" width="50" height="50"></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px" href="index.php">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px; text-shadow: #FFFFFF 0 0 10px;" href="goods.php">Товары</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px" href="contacts.php">Контакты</a>
                        </li>
                    </ul>
                    <?php
                    if (!isset($_SESSION['email_user']))
                    {
                        echo "<a class='nav-link text-white' href='./user_area/user_login.php'><img src='./images/корзина.png' width='50' height='50'></a>
                        <a class='nav-link' href='./user_area/user_login.php'><img src='./images/личный кабинет.png' width='50' height='50'></a>";
                    }
                    else
                    {
                        echo "<a class='nav-link' href='profile.php'><img src='./images/профиль.png' width='50' height='50'></a>
                        <a class='nav-link text-white' href='cart.php'><img src='./images/корзина.png' width='50' height='50'>"; count_items(); echo "</a>
                        <a class='nav-link' href='./user_area/user_logout.php'><img src='./images/выход.png' width='50' height='50'></a>";
                    }
                    ?>
                    <form class="d-flex" action="search_item.php" method="get">
                        <input class="form-control me-2" type="search" placeholder="Поиск" aria-label="Search" name="search_data">
                        <input type="submit" value="Поиск" class="btn btn-outline-light" name="search_data_item">
                    </form>
                </div>
            </div>
        </nav>
    </div>
        <?php
        cart()
        ?>

        <div class="container-fluid">
        </br>
            <h2><strong>Личная информация</strong></h2>
            <?php
            if (isset($_SESSION['email_user']))
            {
                $email_user = $_SESSION['email_user'];
                $select_query="select * from `users` where email_user='$email_user'";
                $result_query=mysqli_query($con, $select_query);
                $run_query = mysqli_fetch_array($result_query);
                $name_user=$run_query['name_user'];
                $surname_user=$run_query['surname_user'];
                $mobile_user=$run_query['mobile_user'];
                echo "<p>$name_user $surname_user</p>
                <p>Электронная почта: $email_user</p>
                <p>Номер телефона: $mobile_user</p>";
            }
            ?>
            </br>
            </h4>
            <p>Заказ можно забрать по адресу: <a href='contacts.php'>г.Пермь, ул.Василия Васильева 17/1</a></p>
            <p>Отмените заказ по номеру: <a href="tel:+79068777500">+79068777500</a></p>
            <h2><strong>Мои заказы</strong></h2>
            <?php
            echo "
                <div class='container'>
                    <div class='row'>
                        <table class='table table-bordered'>
                            <thead>
                                <tr>
                                    <th>Товар</th>
                                    <th>Количество</th>
                                    <th>Сумма</th>
                                    <th>Дата</th>
                                    <th>Статус</th>
                                </tr>
                            </thead>
                            <tbody>";
            $email_user = $_SESSION['email_user'];
            $cart_query = "select * from `orders` where email_user='$email_user' order by date DESC";
            $result = mysqli_query($con, $cart_query);
            while ($row=mysqli_fetch_array($result))
            {
                $id_item = $row['id_item'];
                $select_items = "select * from `goods` where id_item='$id_item'";
                $result_items = mysqli_query($con, $select_items);
                $run_query = mysqli_fetch_array($result_items);
                $name_item=$run_query['name_item'];
                $quantity=$row['quantity'];
                $total=$row['total'];
                $date=$row['date'];
                $status=$row['status'];
                $select_items = "select * from `status_order` where id_status=$status";
                $result_items = mysqli_query($con, $select_items);
                $run_query = mysqli_fetch_array($result_items);
                $name_status=$run_query['name_status'];
                echo "
                        <tr>
                            <th>$name_item</th>
                            <th>$quantity</th>
                            <th>$total ₽</th>
                            <th>$date</th>";
                            if ($status==1)
                            {
                                echo "<th><font color='grey'>$name_status</font></th>";
                            }
                            else if ($status==2)
                            {
                                echo "<th><font color='blue'>$name_status</font></th>";
                            }
                            else if ($status==4)
                            {
                                echo "<th><font color='green'>$name_status</font></th>";
                            }
                            else
                            {
                                echo "<th><font color='red'>$name_status</font></th>";
                            }
                            echo "</tr>";
            }
            echo "</tbody>
            </table>
            </div>
            </div>";
            ?>
        </div>
        <div class="container">
            <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
                <div class="col-md-4 d-flex align-items-center">
                <span class="text-muted">© 2023 Автомир</span>
                </div>
                <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
                    <li class="ms-3"><a href="https://vk.com/public218490604"><img src="./images/вк.png" alt="" width="50" height="50"></a></li>
                    <li class="ms-3"><a href="tel: +79068777500"><img src="./images/телефон.png" alt="" width="50" height="50"></a></li>
                    <li class="ms-3"><a href="mailto:IPGalimovRH@gmail.com"><img src="./images/почта.png" alt="" width="50" height="50"></a></li>
                </ul>
            </footer>
        </div>
<!-- bootstrap js link -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>