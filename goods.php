<?php
include('includes/connect.php');
include('functions/common_function.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Товары | АВТОМИР</title>
    <!-- bootstrap CSS link -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- font awesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- css file -->
    <link rel="stylesheet" href="style.css">
</head>
<body style="font-family: Arial; overflow-x: hidden">
    <!-- navbar -->
    <div class="container-fluid p-0">
        <!-- first part -->
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #4682B4">
            <div class="container-fluid">
                <a href="index.php"><img src="./images/логотип.png" alt="" width="50" height="50"></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px" href="index.php">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px; text-shadow: #FFFFFF 0 0 10px;" href="goods.php">Товары</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#FFFFFF; font-size: 20px" href="contacts.php">Контакты</a>
                        </li>
                    </ul>
                    <?php
                    if (!isset($_SESSION['email_user']))
                    {
                        echo "<a class='nav-link text-white' href='./user_area/user_login.php'><img src='./images/корзина.png' width='50' height='50'></a>
                        <a class='nav-link' href='./user_area/user_login.php'><img src='./images/личный кабинет.png' width='50' height='50'></a>";
                    }
                    else
                    {
                        echo "<a class='nav-link' href='profile.php'><img src='./images/профиль.png' width='50' height='50'></a>
                        <a class='nav-link text-white' href='cart.php'><img src='./images/корзина.png' width='50' height='50'>"; count_items(); echo "</a>
                        <a class='nav-link' href='./user_area/user_logout.php'><img src='./images/выход.png' width='50' height='50'></a>";
                    }
                    ?>
                    <form class="d-flex" action="search_item.php" method="get">
                        <input class="form-control me-2" type="search" placeholder="Поиск" aria-label="Search" name="search_data">
                        <input type="submit" value="Поиск" class="btn btn-outline-light" name="search_data_item">
                    </form>
                </div>
            </div>
        </nav>

        <?php
        cart()
        ?>

        <!-- third part -->
        <div class="row px-3 p-2">
            <div class="col-md-2 p-0">
                <!-- Боковая навигация -->
                <ul class="navbar-nav me-auto">
                    <?php
                    get_categories();
                    ?>
                </ul>
            </div>
            <div class="col-md-10">
                <!-- Товары -->
                <div class="row">
                    <?php
                    get_goods();
                    get_unique_category()
                    ?>
                </div>
            </div>
        </div>
    </div>
        <div class="container">
            <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
                <div class="col-md-4 d-flex align-items-center">
                <span class="text-muted">© 2023 Автомир</span>
                </div>
                <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
                    <li class="ms-3"><a href="https://vk.com/public218490604"><img src="./images/вк.png" alt="" width="50" height="50"></a></li>
                    <li class="ms-3"><a href="tel: +79068777500"><img src="./images/телефон.png" alt="" width="50" height="50"></a></li>
                    <li class="ms-3"><a href="mailto:IPGalimovRH@gmail.com"><img src="./images/почта.png" alt="" width="50" height="50"></a></li>
                </ul>
            </footer>
        </div>
<!-- bootstrap js link -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>