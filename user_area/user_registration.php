<?php
include('../includes/connect.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Регистрация</title>
    <!-- bootstrap CSS link -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- font awesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- css file -->
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container-fluid m-3">
        <h2 class="text-center">Регистрация</h2>
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-lg-12 col-xl-6">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-outline mb-4">
                        <label for="name_user" class="form-label">Имя</label>
                        <input type="text" id="name_user" class="form-control" placeholder="Введите имя" autocomplete="off" required="required" name="name_user"/>
                    </div>
                    <div class="form-outline mb-4">
                        <label for="surname_user" class="form-label">Фамилия</label>
                        <input type="text" id="surname_user" class="form-control" placeholder="Введите фамилию" autocomplete="off" required="required" name="surname_user"/>
                    </div>
                    <div class="form-outline mb-4">
                        <label for="email_user" class="form-label">E-mail</label>
                        <input type="email" id="email_user" class="form-control" placeholder="Введите адрес электронной почты" autocomplete="off" required="required" name="email_user"/>
                    </div>
                    <div class="form-outline mb-4">
                        <label for="mobile_user" class="form-label">Номер телефона</label>
                        <input type="tel" id="mobile_user" class="form-control" placeholder="Введите номер телефона в формате: +79000000000" pattern="\+7\s?[\(]{0,1}9[0-9]{2}[\)]{0,1}\s?\d{3}[-]{0,1}\d{2}[-]{0,1}\d{2}" autocomplete="off" required="required" name="mobile_user"/>
                    </div>
                    <div class="form-outline mb-4">
                        <label for="password_user" class="form-label">Пароль</label>
                        <input type="password" id="password_user" class="form-control" placeholder="Придумайте пароль" autocomplete="off" required="required" name="password_user"/>
                    </div>
                    <div class="form-outline mb-4">
                        <label for="password_check" class="form-label">Повторите пароль</label>
                        <input type="password" id="password_check" class="form-control" placeholder="Повторите пароль" autocomplete="off" required="required" name="password_check"/>
                    </div>
                    <div class="mt-4 pt-2">
                        <input type="submit" name="user_registration" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Зарегистрироваться">
                        <p class="small fw-bold mt-2 pt-1 mb-0">Уже есть аккаунт?<a href="user_login.php"> Войти в личный кабинет</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

<?php
if(isset($_POST['user_registration']))
{
    $name_user = $_POST['name_user'];
    $surname_user = $_POST['surname_user'];
    $email_user = $_POST['email_user'];
    $mobile_user = $_POST['mobile_user'];
    $password_user = $_POST['password_user'];
    $hash_password = password_hash($password_user, PASSWORD_DEFAULT);
    $password_check = $_POST['password_check'];

    $select_query = "select * from `users` where email_user = '$email_user'";
    $result = mysqli_query($con, $select_query);
    $row_count = mysqli_num_rows($result);
    if ($row_count > 0)
    {
        echo "<script>alert('Пользователь с данной электронной почтой уже зарегистрирован в системе')</script>";
    }
    else if ($password_user != $password_check)
    {
        echo "<script>alert('Пароли не совпадают')</script>";
    }
    else
    {
        $insert_query = "insert into `users` (name_user, surname_user, email_user, mobile_user, password_user) values ('$name_user', '$surname_user', '$email_user', '$mobile_user', '$hash_password')";
        $sql_execute=mysqli_query($con, $insert_query);
        $_SESSION['email_user'] = $email_user;
        echo "<script>alert('Успешная регистрация')</script>";
        echo "<script>window.open('../goods.php', '_self')</script>";
    }
}
?>