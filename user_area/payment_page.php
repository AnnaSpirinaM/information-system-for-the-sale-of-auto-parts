<?php
include('../includes/connect.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Вход в личный кабинет</title>
    <!-- bootstrap CSS link -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- font awesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- css file -->
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <h2 class="text-center">Проверьте ваш заказ и оплатите его</h2>
        <div class="row d-flex align-items-center justify-content-center">
            <div>
            <?php
            echo "
                <div class='container'>
                    <div class='row'>
                        <table class='table table-bordered'>
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Товар</th>
                                    <th>Изображение</th>
                                    <th>Цена за штуку</th>
                                    <th>Количество</th>
                                    <th>Итого</th>
                                </tr>
                            </thead>
                            <tbody>";
            $email_user = $_SESSION['email_user'];
            $total = 0;
            $cart_query = "select * from `cart` where email_user='$email_user'";
            $result = mysqli_query($con, $cart_query);
            $count = 0;
            while ($row=mysqli_fetch_array($result))
            {
                $id_item = $row['id_item'];
                $select_items = "select * from `goods` where id_item='$id_item'";
                $result_items = mysqli_query($con, $select_items);
                $count++;
                $quantity=$row['quantity'];
                while ($row_item_price=mysqli_fetch_array($result_items))
                {
                    $price_mas = array($row_item_price['price']);
                    $price_table = $row_item_price['price'];
                    $id_item = $row_item_price['id_item'];
                    $name_item = $row_item_price['name_item'];
                    $image = $row_item_price['image'];
                    $price = array_sum($price_mas);
                    $total_price = $price * $quantity;
                    $total += $total_price;
                    echo "
                                <tr>
                                    <th>$count</th>
                                    <th>$name_item</th>
                                    <th><img src='../admin_area/goods_images/$image' style = 'width: 80px; height: 80px; object-fit: contain'></th>
                                    <th>$price ₽</th>
                                    <th>$quantity</th>
                                    <th>$total_price</th>
                                </tr>";
                }
            }
            echo "
            </tbody>
            </table>
            <div>
                <h4>
                    <a>Итого к оплате: <strong>$total ₽  </strong></a>
            </div>
        </div>
    </div>
    <div class='row d-flex align-items-center justify-content-center'>
            <div class='col-lg-12 col-xl-6'>
                <form action='' method='post' enctype='multipart/form-data'>
                    <div class='form-outline my-4 text-center w-50 m-auto'>
                        <label for='ccn'>Номер карты</label>
                        <input id='ccn' type='tel' name='ccn' class='text-center' pattern='[0-9\s]{13,19}' maxlength='19' placeholder='XXXX XXXX XXXX XXXX'>
                    </div>
                    <div class='my-4 text-center w-50 m-auto'>
                        <input type='submit' name='pay' class='btn mb-3 px-3' style='background-color: #4682B4; color: #FFFFFF' value='Оплатить $total ₽'>
                    </div>
                </form>
            </div>
        </div>";
            ?>
            </div>
        </div>
    </div>
</body>
</html>

<?php
if(isset($_POST['pay']))
{
    $ccn = $_POST['ccn'];
    if($ccn!=null)
    {
        $email_user = $_SESSION['email_user'];
        $cart_query = "select * from `cart` where email_user='$email_user'";
        $result = mysqli_query($con, $cart_query);
        while ($row=mysqli_fetch_array($result))
        {
            $id_item = $row['id_item'];
            $quantity = $row['quantity'];
            $select_query="select * from `goods` where id_item=$id_item";
            $result_query=mysqli_query($con, $select_query);
            $run_query = mysqli_fetch_array($result_query);
            $price=$run_query['price'];
            $quantity_goods=$run_query['quantity'];
            $total = $quantity * $price;
            $insert_query="insert into `orders` (email_user, id_item, quantity, total, status) values ('$email_user', $id_item, $quantity, $total, 1)";
            $new_quantity = $quantity_goods - $quantity;
            $remove_query="update `goods` SET quantity=$new_quantity where id_item=$id_item";
            $result_insert=mysqli_query($con, $insert_query);
            $result_remove=mysqli_query($con, $remove_query);
        }
        $delete_query="delete from `cart` where email_user='$email_user'";
        $result_delete=mysqli_query($con, $delete_query);
        if($result_delete)
        {
            echo "<script>window.open('../profile.php', '_self')</script>";
        }
    }
    else
    {
        echo "<script>alert('Для завершения заказа заполните поле ввода номера карты')</script>";
    }
}
?>