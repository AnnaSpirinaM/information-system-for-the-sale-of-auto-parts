<?php
include('../includes/connect.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Вход в личный кабинет</title>
    <!-- bootstrap CSS link -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- font awesome link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- css file -->
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container-fluid m-3">
        <h2 class="text-center">Вход</h2>
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-lg-12 col-xl-6">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-outline mb-4">
                        <label for="email_user" class="form-label">E-mail</label>
                        <input type="email" id="email_user" class="form-control" placeholder="Введите адрес электронной почты" autocomplete="off" required="required" name="email_user"/>
                    </div>
                    <div class="form-outline mb-4">
                        <label for="password_user" class="form-label">Пароль</label>
                        <input type="password" id="password_user" class="form-control" placeholder="Придумайте пароль" autocomplete="off" required="required" name="password_user"/>
                    </div>
                    <div class="mt-4 pt-2">
                        <input type="submit" name="user_login" class="btn mb-3 px-3" style='background-color: #4682B4; color: #FFFFFF' value="Войти">
                        <p class="small fw-bold mt-2 pt-1 mb-0">Ещё нет аккаунта?<a href="user_registration.php"> Зарегистрироваться</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

<?php
if(isset($_POST['user_login']))
{
    $email_user = $_POST['email_user'];
    $password_user = $_POST['password_user'];
    $select_query="select * from `users` where email_user='$email_user'";
    $result = mysqli_query($con, $select_query);
    $row_count = mysqli_num_rows($result);
    $row_data = mysqli_fetch_assoc($result);
    if ($row_count >0)
    {
        if (password_verify($password_user, $row_data['password_user']))
        {
            $_SESSION['email_user'] = $email_user;
            echo "<script>alert('Успешный вход в систему')</script>";
            echo "<script>window.open('../cart.php', '_self')</script>";
        }
        else
        {
            echo "<script>alert('Неверно введён пароль')</script>";
        }
    }
    else
    {
        echo "<script>alert('Пользователь с введённой электронной почтой не найден')</script>";
    }
}
?>